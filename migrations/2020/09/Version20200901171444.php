<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200901171444 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'file.overview', 'hash' => 'c2e7453a0649ae3882b8a0906c11acbe', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Dokumenty', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.file.title', 'hash' => '767edc94c6eaad5db4bc2eefedf7d9c2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Dokumenty', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.file.description', 'hash' => 'ac9100dce91948e6c92394f0f773e5a7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje spravovat dokumenty včetně historie', 'plural1' => '', 'plural2' => ''],
            ['original' => 'file.overview.title', 'hash' => 'cc652584e806357ca9a504b4aedf9d44', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Dokumenty|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.file.overview.name', 'hash' => '0bc307a3d802ff5d81d62f331b3b31d3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.file.overview.is-important', 'hash' => 'a3be343ca2653f7dcc5b92a1618d478f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Důležitý', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.file.overview.size-in-unit', 'hash' => 'd13bd46331cb8c98b2d9f128c8687ecb', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Velikost', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.file.overview.created-at', 'hash' => '0316560e3816929806802e88d6077c1e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vytvořeno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.file.overview.last-update-at', 'hash' => 'c6ab55da4f25b9a3202623f88862c83f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Poslední úprava', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.file.overview.tags', 'hash' => 'e2e6845c997c583710140e12725b3e12', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Štítky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.file.overview.action.overview-tag', 'hash' => 'a9ce1fdaa6c1b5cc3a6a8a16e8a37c37', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přehled štítků', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.file.overview.action.new', 'hash' => '5656e32df52b93f5e3a91e05c83aeee6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit dokument', 'plural1' => '', 'plural2' => ''],
            ['original' => 'file.edit.title', 'hash' => '0de3d3fc2567dcfb3c1e773addd5c71e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení dokumentu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file.edit.general', 'hash' => '1a3e7beb6e4d5803033ce0f8015cd026', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Nastavení dokumentu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file.edit.file.download', 'hash' => '8fb9f96ea5059a6d4065ab1d0183a539', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Stáhnout', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file.edit.name', 'hash' => 'e924409ed596570a8b6f969b921428f2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file.edit.name.req', 'hash' => '7b3b4ca63fc38e2641c867e9a2f625a8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file.edit.content', 'hash' => 'cffa5b9626a1ef4ccf9f4ba1534e7f01', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file.edit.file', 'hash' => 'ec7e55884bd69043503a51162c16ef7e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Dokument', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file.edit.file.req', 'hash' => '65e318993e67714ec890e03396782bdc', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vyberte prosím dokument', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.upload.file', 'hash' => 'fe213cb4119c88fe52afbcf96c0be088', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vyberte dokument', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file.edit.image-preview', 'hash' => '0f7ebca1ea23258aed640bac4b817149', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Náhled dokumentu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file.edit.image-preview.rule-image', 'hash' => '2ff7965d9e1cc0b66346e3b2b4f9ec55', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file.edit.is-important', 'hash' => 'bea8d4e9b7429cafa2c1a013c5f70979', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je důležitý', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file.edit.is-active', 'hash' => '1bc1f240a438bd120d75ba6b23a9c7ca', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file.edit.send', 'hash' => '68010dc58b19163042a28e2e07dce531', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file.edit.send-back', 'hash' => '16747d99efc4ba1381dac268e0a1f985', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file.edit.back', 'hash' => '900e1b64bd460c30adadcb73f1545a08', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.file.overview.action.edit', 'hash' => 'e8abdcd0889a73241f4dcfc01199d6ba', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.file.overview.action.download-file', 'hash' => '94690f99184917b419ad1df88550717d', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'file.edit.title - %s', 'hash' => '2e5c4fa55920f672509f97a14d6481a9', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace souboru', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.file.overview-tag.action.overview', 'hash' => '336a6eb1dd9beb015d8b0bde41b051c1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přehled dokumentů', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.file.overview-tag.action.new', 'hash' => '130647889e9ff708bf57f3d888b43c56', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit štítek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.file.overview-tag.name', 'hash' => '09ef034062a9992dfc216925c0c10b81', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.file.overview-tag.color', 'hash' => '4c62016145316e7901a1104ad867bb71', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Barva', 'plural1' => '', 'plural2' => ''],
            ['original' => 'file-tag.edit.title', 'hash' => 'eb8cb9fdd035eca05bedd6106517246b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení štítku dokumentu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file-tag.edit.name', 'hash' => 'd49225d35e0d15002ee42b70f7e768da', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file-tag.edit.name.req', 'hash' => '8319c1fc57f76c13903ca7f2ea24f0d2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file-tag.edit.color', 'hash' => '0f943cc05d8dbf2ec77743e17cd16f0d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Barva', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file-tag.edit.content', 'hash' => 'c0d36140dfb87b80be45177b87912441', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Obsah', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file-tag.edit.is-active', 'hash' => 'ba8baff252bf918768c9a07dd176ff28', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file-tag.edit.send', 'hash' => 'a6d9406a34494bfb574766b6bf5a79ea', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file-tag.edit.send-back', 'hash' => 'cf20650e09bc5510ebcca04dbc08f351', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file-tag.edit.back', 'hash' => 'cce2d3e86677d831d9f9d70b1e204118', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'file-tag.edit.title - %s', 'hash' => '761eac276177e49a836f7110d5a3d098', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace štítku dokumentu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file-tag.edit.flash.success.create', 'hash' => '4039cac1a09a4c070776ab522c931d72', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Štítek dokumentu byl úspěšně založen.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.file.overview-tag.action.edit', 'hash' => '6f3174633f6e39235733b22bd86351da', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file.edit.flash.success.update', 'hash' => 'cbd49c5c94b4ac846c2e4ebea9293577', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Dokument byl úspěšně upraven.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file-tag.edit.flash.success.update', 'hash' => '59e2f5c75abf0055e08da3a6156abbf7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Štítek dokumentu byl úspěšně upraven.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file.edit.history', 'hash' => 'f4807f5c85e55627de2811ce639e7dd1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Historie', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file.edit.history.name', 'hash' => '69b2f6b20171244d9d79c1f62ef9f4e6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file.edit.history.created-at', 'hash' => '9562432aadc07d698120712ff1e12808', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vytvořeno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file.edit.history.action', 'hash' => '3ab268cff1b116a51fced7fc3d104293', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Akce', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.file.overview.action.remove', 'hash' => '0544f196c2c91b736c1b5f2d08e4fbb8', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.file.overview.action.remove %%s', 'hash' => '018836caa057711d0b44fbc3d677c7e6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Opravdu chcete odebrat dokument "%%s"?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.file.overview.action.flash.remove.success', 'hash' => 'bbdcab74e014f4e0db33545bc4c8cd49', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Dokument byl úspěšně odebrán.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.file.overview-tag.action.flash.sort.success', 'hash' => 'd3c1ff8641565c06b95cfdc93e49560d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pořadí štítků dokumentů bylo upraveno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file.edit.tags', 'hash' => '50724ac66b6cd1cab3f8aeadbbb2da2a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Štítky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file.edit.tags.placeholder', 'hash' => '35a8ee49d3a8bc977d9078a99c031bfe', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vyberte štítky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.file.edit.tags.no-result', 'hash' => '5b9063b2456d4501e15c097dc2cfa4f1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Nebyl nalezen žádný štítek: ', 'plural1' => '', 'plural2' => ''],
            ['original' => 'file.overview-tag.title', 'hash' => 'ada3e795320c93b51ed11b2ce1f23401', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Štítky dokumentů|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.file.overview-tag.is-active', 'hash' => '80bebd0210b691e95800dc4c153c3b65', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
