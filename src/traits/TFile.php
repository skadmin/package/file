<?php

declare(strict_types=1);

namespace Skadmin\File\Traits;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Skadmin\File\Doctrine\File\File;

trait TFile
{

    /** @var Collection<int, File> */
    #[ORM\ManyToMany(targetEntity: File::class)]
    protected Collection $files;

    /**
     * @return Collection<int, File>
     */
    public function getFiles(bool $onlyActive = false, ?string $orderBy = 'lastUpdateAt', string $ordering = Criteria::DESC): Collection
    {
        $criteria = Criteria::create();

        $criteriaOrderBy = ['isImportant' => Criteria::DESC];
        if ($orderBy !== null) {
            $criteriaOrderBy[$orderBy] = $ordering;
        }

        $criteria->orderBy($criteriaOrderBy);

        if ($onlyActive) {
            $criteria->andWhere(Criteria::expr()->eq('isActive', true));
        }

        return $this->files->matching($criteria);
    }

    public function addFile(File $file): void
    {
        if ($this->files->contains($file)) {
            return;
        }

        $this->files->add($file);
    }

    public function removeFileByHash(string $hash): void
    {
        $file = $this->getFileByHash($hash);

        if ($file === null) {
            return;
        }

        $this->files->removeElement($file);
    }

    public function getFileByHash(string $hash): ?File
    {
        $criteria = Criteria::create()->where(Criteria::expr()->eq('hash', $hash));
        $file     = $this->files->matching($criteria)->first();

        return $file ? $file : null;
    }
}
