<?php

declare(strict_types=1);

namespace Skadmin\File\Traits;

use Nette\Utils\Arrays;
use Skadmin\File\Doctrine\File\File;

trait TFileIdentifier
{
    use TFile;

    /**
     * @return array<mixed>
     */
    public function getFilesIdentifiers(): array
    {
        return Arrays::map($this->getFiles(), static fn (File $file): string => $file->getIdentifier());
    }
}
