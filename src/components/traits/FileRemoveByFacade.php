<?php

declare(strict_types=1);

namespace Skadmin\File\Components\Admin;

trait FileRemoveByFacade
{
    use FileInit;

    public function handleRemoveFile(string $hash): void
    {
        $this->checkFileObject();

        $this->facade->removeFileByHash($this->fileObject, $hash);
        $this->formFileOnRedraw();
    }
}
