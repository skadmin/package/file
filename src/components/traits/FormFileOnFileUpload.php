<?php

declare(strict_types=1);

namespace Skadmin\File\Components\Admin;

use Skadmin\File\Doctrine\File\File;

trait FormFileOnFileUpload
{
    use FileInit;

    public function formFileOnFileUpload(File $file): void
    {
        $this->checkFileObject();

        $this->facade->addFile($this->fileObject, $file);
    }
}
