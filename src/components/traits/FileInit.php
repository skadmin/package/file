<?php

declare(strict_types=1);

namespace Skadmin\File\Components\Admin;

use App\Model\Exception\ExceptionFileObjectIsNull;
use App\Model\Exception\ExceptionFileStorageIsNull;
use Skadmin\FileStorage\FileStorage;

trait FileInit
{
    private mixed        $fileObject  = null;
    private ?FileStorage $fileStorage = null;

    private function checkFileObject(): void
    {
        if ($this->fileObject === null) {
            throw new ExceptionFileObjectIsNull('File object is null, you must set valid type');
        }

        if ($this->fileStorage === null) {
            throw new ExceptionFileStorageIsNull('File storage is null, you must set');
        }
    }
}
