<?php

declare(strict_types=1);

namespace Skadmin\File\Components\Admin;

trait FileDownloadByFacade
{
    use FileInit;

    public function handleDownloadFile(string $hash): void
    {
        $this->checkFileObject();

        $file = $this->facade->getFileByHash($this->fileObject, $hash);

        if ($file !== null) {
            /* @phpstan-ignore-next-line */
            $this->fileStorage->download($file->getIdentifier(), $file->getName(), $file->getMimeType(), $file->getSize());
        } else {
            $this->getParent()->redirect(':Admin:Homepage:fileNotFound', ['hash' => $hash]);
        }
    }
}
