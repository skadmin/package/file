<?php

declare(strict_types=1);

namespace Skadmin\File\Components\Admin;

trait CreateComponentFormFile
{
    use FormFileOnRedraw;
    use FormFileOnFileUpload;

    protected function createComponentFormFile(): FormFile
    {
        $this->formFile->onFileUpload[] = [$this, 'formFileOnFileUpload']; //@phpstan-ignore-line
        $this->formFile->onRedraw[]     = [$this, 'formFileOnRedraw'];

        return $this->formFile;
    }
}
