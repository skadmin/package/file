<?php

declare(strict_types=1);

namespace Skadmin\File\Components\Admin;

trait FormFileOnRedraw
{
    public function formFileOnRedraw(): void
    {
        $this->redrawControl('snipDocuments');
        $this->redrawControl('snipDocumentsCount');
    }
}
