<?php

declare(strict_types=1);

namespace Skadmin\File\Components\Front;

interface IOverviewFactory
{
    public function create(): Overview;
}
