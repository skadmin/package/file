<?php

declare(strict_types=1);

namespace Skadmin\File\Components\Front;

use App\Components\Grid\TemplateControl;
use App\Model\System\APackageControl;
use Skadmin\File\Doctrine\File\FileFacade;
use Skadmin\FileStorage\FileStorage;
use Skadmin\Translator\Translator;

class Overview extends TemplateControl
{
    use APackageControl;

    private FileFacade  $facade;
    private FileStorage $fileStorage;

    public function __construct(FileFacade $facade, Translator $translator, FileStorage $fileStorage)
    {
        parent::__construct($translator);
        $this->facade      = $facade;
        $this->fileStorage = $fileStorage;
    }

    public function getTitle(): string
    {
        return 'file.front.overview.title';
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');

        $template->files = $this->facade->getImportantFiles();

        $template->render();
    }

    public function handleDownloadFile(string $hash): void
    {
        $file = $this->facade->getByHash($hash);

        if ($file !== null && $file->getIdentifier() !== null) {
            $this->fileStorage->download($file->getIdentifier(), $file->getName(), $file->getMimeType(), $file->getSize());
        } else {
            $this->getParent()->redirect(':Admin:Homepage:fileNotFound', ['hash' => $hash]);
        }
    }
}
