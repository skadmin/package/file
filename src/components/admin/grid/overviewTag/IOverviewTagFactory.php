<?php

declare(strict_types=1);

namespace Skadmin\File\Components\Admin;

interface IOverviewTagFactory
{
    public function create(): OverviewTag;
}
