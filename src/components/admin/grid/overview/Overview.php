<?php

declare(strict_types=1);

namespace Skadmin\File\Components\Admin;

use SkadminUtils\GridControls\UI\GridDoctrine;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Html;
use Skadmin\File\BaseControl;
use Skadmin\File\Doctrine\File\File;
use Skadmin\File\Doctrine\File\FileFacade;
use Skadmin\File\Doctrine\File\FileTagFacade;
use Skadmin\FileStorage\FileStorage;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\ImageStorage\ImageStorage;
use SkadminUtils\Utils\Utils\Colors\Colors;
use Ublaboo\DataGrid\Column\Action\Confirmation\StringConfirmation;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;

use function boolval;
use function intval;
use function sprintf;

class Overview extends GridControl
{
    use APackageControl;

    private FileFacade    $facade;
    private FileTagFacade $facadeFileTag;
    private FileStorage   $fileStorage;
    private FormFile      $formFile;
    private ImageStorage  $imageStorage;

    public function __construct(FileFacade $facade, FileTagFacade $facadeFileTag, Translator $translator, User $user, FileStorage $fileStorage, IFormFileFactory $iFormFileFactory, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);

        $this->facade        = $facade;
        $this->facadeFileTag = $facadeFileTag;
        $this->fileStorage   = $fileStorage;
        $this->imageStorage  = $imageStorage;

        $this->formFile = $iFormFileFactory->create();
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return $this->formFile->getCss();
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return $this->formFile->getJs();
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'file.overview.title';
    }

    public function handleDownloadFile(string $hash): void
    {
        $file = $this->facade->getByHash($hash);

        if ($file !== null && $file->getIdentifier() !== null) {
            $this->fileStorage->download($file->getIdentifier(), $file->getName());
        } else {
            $this->getParent()->redirect(':Admin:Homepage:fileNotFound', ['hash' => $hash]);
        }
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModelForGrid());

        // DATA
        $dialYesNo = Constant::DIAL_YES_NO;

        // COLUMNS
        $grid->addColumnText('preview', '')
            ->setRenderer(function (File $file): ?Html {
                if ($file->getImagePreview() !== null) {
                    $imageSrc = $this->imageStorage->fromIdentifier([$file->getImagePreview(), '60x60', 'exact']);

                    return Html::el('img', [
                        'src'   => sprintf('/%s', $imageSrc->createLink()),
                        'style' => 'max-width: none;',
                    ]);
                }

                return null;
            })->setAlign('center');
        $grid->addColumnText('name', 'grid.file.overview.name');
        $grid->addColumnText('tags', 'grid.file.overview.tags')
            ->setRenderer(static function (File $file): Html {
                $tags = new Html();
                foreach ($file->getTags() as $tag) {
                    $color = $tag->getColor() !== '' ? $tag->getColor() : '#FFFFFF';
                    $tags->addHtml(Html::el('span', [
                        'class' => 'badge mr-1 px-2 mb-1 border border-primary',
                        'style' => sprintf('background-color: %s; color: %s', $color, Colors::getContrastColor($color)),
                    ])->setText($tag->getName()));
                }

                return $tags;
            });
        $grid->addColumnText('sizeInUnit', 'grid.file.overview.size-in-unit')
            ->setAlign('right');
        $grid->addColumnDateTime('createdAt', 'grid.file.overview.created-at')
            ->setFormat('d.m.Y H:i');
        $grid->addColumnDateTime('lastUpdateAt', 'grid.file.overview.last-update-at')
            ->setFormat('d.m.Y H:i');
        $grid->addColumnStatus('isImportant', 'grid.file.overview.is-important')
            ->setAlign('center')
            ->addOption(false, $dialYesNo[0])
            ->setClass('btn-outline-danger')
            ->endOption()
            ->addOption(true, $dialYesNo[1])
            ->setClass('btn-danger')
            ->endOption()
            ->onChange[] = [$this, 'gridFileOverviewChangeIsImportant'];

        // STYLE
        $grid->getColumn('preview')
            ->getElementPrototype('th')
            ->setAttribute('style', 'width: 1px');

        // Row callback
        $grid->setRowCallback(static function (File $file, Html $tr): void {
            if (! $file->isImportant()) {
                return;
            }

            //$tr->setAttribute('class', 'font-weight-bold');
        });

        // EDIT IF TRANSLATION ALLOWED WRITE
        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->getColumn('name')->setEditableCallback([$this, 'gridFileOverviewChangeName']);
        }

        // FILTER
        $grid->addFilterText('name', 'grid.file.overview.name');
        $grid->addFilterSelect('tags', 'grid.file.overview.tags', $this->facadeFileTag->getPairs('id', 'name'), 't.id')
            ->setPrompt(Constant::PROMTP);

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.file.overview.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::DELETE)) {
            $grid->addActionCallback('remove', 'grid.file.overview.action.remove', [$this, 'gridFileOverviewRemove'])
                ->setIcon('trash')
                ->setTitle('grid.file.overview.action.remove')
                ->setConfirmation(new StringConfirmation('grid.file.overview.action.remove %%s', 'name'))
                ->setClass('btn btn-xs btn-outline-danger ajax');
        }

        $grid->addAction('download-file', 'grid.file.overview.action.download-file', 'downloadFile!', ['hash' => 'hash'])
            ->setIcon('download')
            ->setClass('btn btn-xs btn-outline-primary');

        // ALLOWED ACTION

        // TOOLBAR
        $grid->addToolbarButton('Component:default#2', 'grid.file.overview.action.overview-tag', [
            'package' => new BaseControl(),
            'render'  => 'overview-tag',
        ])->setIcon('tags')
            ->setClass('btn btn-xs btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addToolbarButton('Component:default', 'grid.file.overview.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        $grid->addToolbarSeo(BaseControl::RESOURCE, $this->getTitle());

        // OTHER
        $grid->setDefaultSort([
            'isImportant'  => 'DESC',
            'lastUpdateAt' => 'DESC',
        ]);

        return $grid;
    }

    public function gridFileOverviewChangeName(int|string $id, string $name): void
    {
        $mutation = $this->facade->updateName(intval($id), $name);

        $presenter = $this->getPresenterIfExists();
        if ($presenter === null) {
            return;
        }

        $presenter->flashMessage('grid.file.overview.flash.change-name', Flash::SUCCESS);
    }

    public function gridFileOverviewChangeIsImportant(string|int $id, string|int $isImportant): void
    {
        $file     = $this->facade->get(intval($id));
        $mutation = $this->facade->updateIsImportant($file, boolval($isImportant));

        $this['grid']->redrawItem($id);
    }

    protected function createComponentFormFile(): FormFile
    {
        $this->formFile->onRedraw[] = [$this, 'formFileOnRedraw'];

        return $this->formFile;
    }

    public function formFileOnRedraw(): void
    {
        $this->redrawControl('snipGrid');
    }

    public function gridFileOverviewRemove(string $fileId): void
    {
        $file      = $this->facade->get(intval($fileId));
        $presenter = $this->getPresenterIfExists();

        if (! $file->isLoaded()) {
            if ($presenter !== null) {
                $presenter->flashMessage('grid.file.overview.action.flash.remove.fail', Flash::DANGER);
            }
        } else {
            $this->facade->remove($file);

            if ($presenter !== null) {
                $presenter->flashMessage('grid.file.overview.action.flash.remove.success', Flash::SUCCESS);
            }
        }

        $this['grid']->reload();
    }
}
