<?php

declare(strict_types=1);

namespace Skadmin\File\Components\Admin;

use SkadminUtils\FormControls\UI\FormControl;
use App\Components\Form\FormDropzone;
use App\Components\Form\IFormDropzoneFactory;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\Application\Responses\JsonResponse;
use Nette\Http\FileUpload;
use Nette\Utils\ArrayHash;
use Nette\Utils\Arrays;
use Skadmin\File\BaseControl;
use Skadmin\File\Doctrine\File\File;
use Skadmin\File\Doctrine\File\FileFacade;
use Skadmin\File\Doctrine\File\FileFilter;
use Skadmin\FileStorage\FileStorage;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function sprintf;

class FormFile extends FormControl
{
    use APackageControl;

    /** @var callable[]&(callable(File): void)[]; */
    public array          $onFileUpload;
    private FormDropzone  $formDropzoneFactory;
    private FileFacade    $facade;
    private FileStorage   $fileStorage;
    private LoaderFactory $webLoader;

    public function __construct(Translator $translator, FileFacade $facade, IFormDropzoneFactory $iFormDropzoneFactory, FileStorage $fileStorage, LoaderFactory $webLoader)
    {
        parent::__construct($translator);
        $this->facade = $facade;

        $this->fileStorage         = $fileStorage;
        $this->webLoader           = $webLoader;
        $this->formDropzoneFactory = $iFormDropzoneFactory->create();
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        $csss   = $this->formDropzoneFactory->getCss();
        $csss[] = $this->webLoader->createCssLoader('jQueryUi');

        return $csss;
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        $jss   = $this->formDropzoneFactory->getJs();
        $jss[] = $this->webLoader->createJavaScriptLoader('jQueryUi');

        return $jss;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();

        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/formFile.latte');
        $template->showSearch = ! $this->getParent() instanceof Overview;

        $template->render();
    }

    public function formDropzoneOnFileUpload(FileUpload $fileUpload): void
    {
        $identifier = $this->fileStorage->save($fileUpload, BaseControl::DIR_FILE);
        $file       = $this->facade->create($fileUpload->getName(), $identifier, $fileUpload->getSize(), $fileUpload->getContentType() ?? '');

        $this->onFileUpload($file);
    }

    public function formDropzoneOnRedraw(): void
    {
        $this->onRedraw();
    }

    protected function createComponentFormDropzone(): FormDropzone
    {
        $this->formDropzoneFactory->onFileUpload[] = [$this, 'formDropzoneOnFileUpload']; //@phpstan-ignore-line
        $this->formDropzoneFactory->onRedraw[]     = [$this, 'formDropzoneOnRedraw'];

        return $this->formDropzoneFactory;
    }

    protected function createComponentFormSearch(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        $form->addHidden('fileHash');
        $form->addText('phrase', 'form-search.file.form-file.phrase')
            ->setHtmlAttribute('placeholder', 'form-search.file.form-file.phrase.placeholder');
        $form->addSubmit('send', 'form-search.file.form-file.send');
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        $file = $this->facade->getByHash($values->fileHash);
        if ($file instanceof File) {
            $this->flashMessage('form-search.file.form-file.flashmessage.success', Flash::SUCCESS);
            $this->onFileUpload($file);
        } else {
            $this->flashMessage('form-search.file.form-file.flashmessage.danger', Flash::DANGER);
        }

        $form->reset();
        $this->redrawControl('snipForm');
        $this->redrawControl('snipFlashmessage');
        $this->onRedraw();
    }

    public function handleSearchFile(?string $phrase = null): JsonResponse
    {
        $filter = new FileFilter($phrase);

        $files = Arrays::map($this->facade->find($filter, 30), static fn (File $f): array => [
            'id'    => $f->getId(),
            'hash'  => $f->getHash(),
            'label' => sprintf('%s [%s]', $f->getName(), $f->getSizeInUnit()),
            'value' => sprintf('%s [%s]', $f->getName(), $f->getSizeInUnit()),
        ]);

        return $this->getPresenter()->sendResponse(new JsonResponse($files));
    }
}
