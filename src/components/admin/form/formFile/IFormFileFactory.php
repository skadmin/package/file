<?php

declare(strict_types=1);

namespace Skadmin\File\Components\Admin;

interface IFormFileFactory
{
    public function create(): FormFile;
}
