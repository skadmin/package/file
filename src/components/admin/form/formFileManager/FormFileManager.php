<?php

declare(strict_types=1);

namespace Skadmin\File\Components\Admin;

use SkadminUtils\DoctrineTraits\Facade;
use SkadminUtils\FormControls\UI\FormControl;
use App\Model\System\APackageControl;
use Nette\ComponentModel\IContainer;
use Nette\DI\Container;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\FileStorage\FileStorage;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function explode;
use function method_exists;
use function sprintf;

class FormFileManager extends FormControl
{
    use APackageControl;
    use CreateComponentFormFile;
    use FileDownloadByFacade {
        CreateComponentFormFile::checkFileObject insteadof FileDownloadByFacade;
    }
    use FileRemoveByFacade {
        CreateComponentFormFile::checkFileObject insteadof FileRemoveByFacade;
    }

    /** @persistent */
    public ?string                 $identifier = null;
    private EntityManagerDecorator $em;
    private Container              $container;
    private Facade                 $facade;
    private LoaderFactory          $webLoader;
    private FormFile               $formFile;
    private string                 $title      = '';

    public function __construct(EntityManagerDecorator $em, Container $container, Translator $translator, LoaderFactory $webLoader, IFormFileFactory $iFormFileFactory, FileStorage $fileStorage)
    {
        parent::__construct($translator);
        $this->em          = $em;
        $this->container   = $container;
        $this->webLoader   = $webLoader;
        $this->fileStorage = $fileStorage;

        $this->formFile = $iFormFileFactory->create();
        $this->isModal  = true;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if ($this->getPresenter()->getParameter('entity') !== null) {
            $entity           = $this->getPresenter()->getParameter('entity');
            $repository       = $this->em->getRepository($entity);
            $this->fileObject = $repository->find($this->getPresenter()->getParameter('entityId'));
            $this->title      = $this->getPresenter()->getParameter('title');

            $this->identifier = sprintf('%s::%s', $this->getPresenter()->getParameter('entity'), $this->getPresenter()->getParameter('entityId'));

            if (! method_exists($this->fileObject, 'getFiles')) {
                $this->fileObject = null;
            }
        } elseif ($this->identifier !== null) {
            [$entity, $entityId] = explode('::', $this->identifier);
            $repository       = $this->em->getRepository($entity);
            $this->fileObject = $repository->find($entityId);
        }

        $this->facade = $this->container->getByType(sprintf('%sFacade', $entity));

        return $this;
    }

    public function getTitle(): SimpleTranslation
    {
        return new SimpleTranslation('file.form-file-manager.title - %s', $this->title);
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return $this->formFile->getCss();
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return $this->formFile->getJs();
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();

        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/formFileManager.latte');
        $template->object = $this->fileObject;

        if ($this->getPresenterIfExists()) {
            $presenter        = $this->getPresenter();
            $template->isAjax = $presenter->isAjax();
        } else {
            $template->isAjax = false;
        }

        $template->render();
    }

    public function formFileOnRedraw(): void
    {
        $this->redrawControl('snipDocuments');
        $this->redrawControl('snipDocumentsCount');

        $this->getPresenter()->redrawControl('snipModal', false);
        $this->getPresenter()->payload->dontHideModalbackdrop = true;
    }
}
