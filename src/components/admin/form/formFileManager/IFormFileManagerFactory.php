<?php

declare(strict_types=1);

namespace Skadmin\File\Components\Admin;

interface IFormFileManagerFactory
{
    public function create(): FormFileManager;
}
