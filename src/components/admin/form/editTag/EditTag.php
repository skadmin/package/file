<?php

declare(strict_types=1);

namespace Skadmin\File\Components\Admin;

use SkadminUtils\FormControls\UI\FormWithUserControl;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\File\BaseControl;
use Skadmin\File\Doctrine\File\FileTag;
use Skadmin\File\Doctrine\File\FileTagFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class EditTag extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory $webLoader;
    private FileTagFacade $facade;
    private FileTag       $fileTag;

    public function __construct(?int $id, FileTagFacade $facade, Translator $translator, LoggedUser $user, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);
        $this->facade    = $facade;
        $this->webLoader = $webLoader;

        $this->fileTag = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->fileTag->isLoaded()) {
            return new SimpleTranslation('file-tag.edit.title - %s', $this->fileTag->getName());
        }

        return 'file-tag.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('colorPicker'),
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('colorPicker'),
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        if ($this->fileTag->isLoaded()) {
            $fileTag = $this->facade->update(
                $this->fileTag->getId(),
                $values->name,
                $values->content,
                $values->color,
                $values->isActive
            );
            $this->onFlashmessage('form.file-tag.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $fileTag = $this->facade->create(
                $values->name,
                $values->content,
                $values->color,
                $values->isActive
            );
            $this->onFlashmessage('form.file-tag.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-tag',
            'id'      => $fileTag->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview-tag',
        ]);
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editTag.latte');

        $template->fileTag = $this->fileTag;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.file-tag.edit.name')
            ->setRequired('form.file-tag.edit.name.req');
        $form->addTextArea('content', 'form.file-tag.edit.content');
        $form->addText('color', 'form.file-tag.edit.color');

        $form->addCheckbox('isActive', 'form.file-tag.edit.is-active')
            ->setDefaultValue(true);

        // BUTTON
        $form->addSubmit('send', 'form.file-tag.edit.send');
        $form->addSubmit('sendBack', 'form.file-tag.edit.send-back');
        $form->addSubmit('back', 'form.file-tag.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->fileTag->isLoaded()) {
            return [];
        }

        return [
            'name'     => $this->fileTag->getName(),
            'content'  => $this->fileTag->getContent(),
            'color'    => $this->fileTag->getColor(),
            'isActive' => $this->fileTag->isActive(),
        ];
    }
}
