<?php

declare(strict_types=1);

namespace Skadmin\File\Components\Admin;

use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Http\FileUpload;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Arrays;
use Skadmin\File\BaseControl;
use Skadmin\File\Doctrine\File\File;
use Skadmin\File\Doctrine\File\FileFacade;
use Skadmin\File\Doctrine\File\FileTag;
use Skadmin\File\Doctrine\File\FileTagFacade;
use Skadmin\FileStorage\FileStorage;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function assert;
use function intval;

class Edit extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory  $webLoader;
    private FileFacade    $facade;
    private FileTagFacade $facadeFileTag;
    private ImageStorage  $imageStorage;
    private FileStorage   $fileStorage;
    private File          $file;

    public function __construct(?int $id, FileFacade $facade, FileTagFacade $facadeFileTag, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, ImageStorage $imageStorage, FileStorage $fileStorage)
    {
        parent::__construct($translator, $user);
        $this->facade        = $facade;
        $this->facadeFileTag = $facadeFileTag;
        $this->webLoader     = $webLoader;
        $this->imageStorage  = $imageStorage;
        $this->fileStorage   = $fileStorage;

        $this->file = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->file->isLoaded()) {
            return new SimpleTranslation('file.edit.title - %s', $this->file->getName());
        }

        return 'file.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function processOnValidate(Form $form, ArrayHash $values): void
    {
        $fileUpload = $values->file;
        assert($fileUpload instanceof FileUpload);
        if (! $fileUpload->hasFile() || $fileUpload->isOk()) {
            return;
        }

        $form->addError('form.file.edit.flash.file.is-wrong');
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        $fileUpload = $values->file;
        assert($fileUpload instanceof FileUpload);

        // IDENTIFIER
        $imageIdentifier = UtilsFormControl::getImagePreview($values->imagePreview, BaseControl::DIR_IMAGE);

        if (isset($values->tags)) {
            $tags = Arrays::map($values->tags, function ($tagId): FileTag {
                return $this->facadeFileTag->get($tagId);
            });
        }

        if ($this->file->isLoaded()) {
            if ($fileUpload->isOk() && $fileUpload->isImage()) {
                $fileUploadAsImage = clone $values->file;
                $imageIdentifier   = $this->imageStorage->saveContent($fileUpload->getContents(), $fileUpload->getSanitizedName(), BaseControl::DIR_IMAGE)->identifier;
            }

            if ($imageIdentifier !== null && $this->file->getImagePreview() !== null) {
                $this->imageStorage->delete($this->file->getImagePreview());
            }

            // Máme nový soubor, tak ho nahrajeme (dojde k automatickéhmu uložení původního do historie)
            if ($fileUpload->isOk()) {
                $fileIdentifier = $this->fileStorage->save($fileUpload, BaseControl::DIR_FILE);
                $this->facade->updateFile($this->file->getId(), $fileIdentifier, $fileUpload->getSize(), $fileUpload->getContentType() ?? '');
            }

            $file = $this->facade->update(
                $this->file->getId(),
                $values->name,
                $values->content,
                $values->isActive,
                $values->isImportant,
                $imageIdentifier,
                $tags ?? $this->file->getTags()->toArray()
            );

            $this->onFlashmessage('form.file.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $fileIdentifier = $this->fileStorage->save($fileUpload, BaseControl::DIR_FILE);
            $file           = $this->facade->create($values->name, $fileIdentifier, $fileUpload->getSize(), $fileUpload->getContentType() ?? '');

            $file = $this->facade->update(
                $file->getId(),
                $values->name,
                $values->content,
                $values->isActive,
                $values->isImportant,
                $imageIdentifier,
                $tags ?? []
            );

            $this->onFlashmessage('form.file.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit',
            'id'      => $file->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ]);
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->imageStorage = $this->imageStorage;
        $template->ffile        = $this->file;

        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.file.edit.name')
            ->setRequired('form.file.edit.name.req');
        $form->addTextArea('content', 'form.file.edit.content');

        // CHECKBOX
        $form->addCheckbox('isImportant', 'form.file.edit.is-important');
        $form->addCheckbox('isActive', 'form.file.edit.is-active')
            ->setDefaultValue(true);

        // TAGS
        $form->addMultiSelect('tags', 'form.file.edit.tags', $this->facadeFileTag->getPairs('id', 'name'))
            ->setTranslator(null);

        // UPLOAD
        $inputFile = $form->addUpload('file', 'form.file.edit.file');
        if (! $this->file->isLoaded()) {
            $inputFile->setRequired('form.file.edit.file.req');
        }

        $form->addImageWithRFM('imagePreview', 'form.file.edit.image-preview');

        // BUTTON
        $form->addSubmit('send', 'form.file.edit.send');
        $form->addSubmit('sendBack', 'form.file.edit.send-back');
        $form->addSubmit('back', 'form.file.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onValidate[] = [$this, 'processOnValidate'];
        $form->onSuccess[]  = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->file->isLoaded()) {
            return [];
        }

        $tagsId = Arrays::map($this->file->getTags()->toArray(), static function (FileTag $tag): ?int {
            return $tag->getId();
        });

        return [
            'name'        => $this->file->getName(),
            'content'     => $this->file->getContent(),
            'isImportant' => $this->file->isImportant(),
            'isActive'    => $this->file->isActive(),
            'tags'        => $tagsId,
        ];
    }

    public function handleDownloadFile(string $hash, ?string $version = null): void
    {
        $file = $this->facade->getByHash($hash);

        if ($version !== null && $file !== null) {
            $file = $file->getVersion(intval($version));
        }

        if ($file !== null && $file->getIdentifier() !== null) {
            $this->fileStorage->download($file->getIdentifier(), $file->getName());
        } else {
            $this->getParent()->redirect(':Admin:Homepage:fileNotFound', ['hash' => $hash]);
        }
    }
}
