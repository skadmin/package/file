<?php

declare(strict_types=1);

namespace Skadmin\File;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'file';
    public const DIR_FILE  = 'file';
    public const DIR_IMAGE = 'file';

    public function getMenu(): ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-copy']),
            'items'   => ['overview'],
        ]);
    }
}
