<?php

declare(strict_types=1);

namespace Skadmin\File\Doctrine\File;

use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;

final class FileTagFacade extends Facade
{
    use Facade\Webalize;
    use Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = FileTag::class;
    }

    public function create(string $name, string $content, string $color, bool $isActive): FileTag
    {
        return $this->update(null, $name, $content, $color, $isActive);
    }

    public function update(?int $id, string $name, string $content, string $color, bool $isActive): FileTag
    {
        $fileTag = $this->get($id);
        $fileTag->update($name, $content, $color, $isActive);

        if (! $fileTag->isLoaded()) {
            $fileTag->setWebalize($this->getValidWebalize($name));
            $fileTag->setSequence($this->getValidSequence());
        }

        $this->em->persist($fileTag);
        $this->em->flush();

        return $fileTag;
    }

    public function get(?int $id = null): FileTag
    {
        if ($id === null) {
            return new FileTag();
        }

        $fileTag = parent::get($id);

        if ($fileTag === null) {
            return new FileTag();
        }

        return $fileTag;
    }

    /**
     * @return FileTag[]
     */
    public function getAll(bool $onlyActive = false): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        $orderBy = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    public function findByWebalize(string $webalize): ?FileTag
    {
        $criteria = ['webalize' => $webalize];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }
}
