<?php

declare(strict_types=1);

namespace Skadmin\File\Doctrine\File;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use SkadminUtils\DoctrineTraits\Entity;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class FileTag
{
    use Entity\Id;
    use Entity\IsActive;
    use Entity\WebalizeName;
    use Entity\Content;
    use Entity\Sequence;

    #[ORM\Column]
    private string $color = '';

    /** @var Collection<File> */
    #[ORM\ManyToMany(targetEntity: File::class, mappedBy: 'tags')]
    #[ORM\OrderBy(['isImportant' => 'DESC', 'name' => 'ASC'])]
    private ArrayCollection|Collection|array $files;

    public function __construct()
    {
        $this->files = new ArrayCollection();
    }

    public function update(string $name, string $content, string $color, bool $isActive): void
    {
        $this->name    = $name;
        $this->content = $content;
        $this->color   = $color;

        $this->setIsActive($isActive);
    }

    public function getColor(): string
    {
        return $this->color === '' ? '#FFFFFF' : $this->color;
    }

    /**
     * @return Collection<File>
     */
    public function getFiles(bool $onlyActive = false, ?string $orderBy = 'name', string $ordering = Criteria::DESC): Collection
    {
        $criteria = Criteria::create();

        if ($orderBy !== null) {
            $criteria->orderBy([
                'isImportant' => Criteria::DESC,
                $orderBy      => $ordering,
            ]);
        }

        if (! $onlyActive) {
            return $this->files->matching($criteria);
        }

        $criteria->where(Criteria::expr()->eq('isActive', true));

        return $this->files->matching($criteria);
    }

}
