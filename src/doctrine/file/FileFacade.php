<?php

declare(strict_types=1);

namespace Skadmin\File\Doctrine\File;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\File\BaseControl;
use Skadmin\FileStorage\FileStorage;
use SkadminUtils\DoctrineTraits\Facade;
use SkadminUtils\ImageStorage\ImageStorage;

use function assert;
use function file_get_contents;
use function intval;

final class FileFacade extends Facade
{
    use Facade\Hash;

    private ImageStorage $imageStorage;
    private FileStorage  $fileStorage;

    public function __construct(EntityManagerDecorator $em, ImageStorage $imageStorage, FileStorage $fileStorage)
    {
        parent::__construct($em);
        $this->imageStorage = $imageStorage;
        $this->fileStorage  = $fileStorage;

        $this->table = File::class;
    }

    public function getModelForGrid(): QueryBuilder
    {
        $repository = $this->em->getRepository($this->table);
        assert($repository instanceof EntityRepository);

        return $repository->createQueryBuilder('a')
            ->leftJoin('a.tags', 't');
    }

    public function getCount(?FileFilter $filter = null): int
    {
        $qb = $this->findQb($filter);
        $qb->select('count(a.id)');

        return intval($qb->getQuery()
            ->getSingleScalarResult());
    }

    /** @return array<File> */
    public function find(?FileFilter $filter = null, ?int $limit = null, ?int $offset = null): array
    {
        $qb = $this->findQb($filter, $limit, $offset);

        return $qb->getQuery()
            ->getResult();
    }

    private function findQb(?FileFilter $filter, ?int $limit = null, ?int $offset = null): QueryBuilder
    {
        $qb = $this->em
            ->getRepository($this->table)
            ->createQueryBuilder('a');
        assert($qb instanceof QueryBuilder);

        $qb->setMaxResults($limit)
            ->setFirstResult($offset)
            ->orderBy('a.name', 'ASC');

        $criteria = Criteria::create();

        if ($filter !== null) {
            $filter->modifyCriteria($criteria);
        }

        return $qb->addCriteria($criteria);
    }

    public function create(string $name, string $identifier, int $size, string $mimeType): File
    {
        $file = $this->get();

        $file->create($name, $identifier, $this->getValidHash(), $size, $mimeType);

        $filePreview = $this->fileStorage->getFilePreview($identifier);
        if ($filePreview->isImage()) {
            $imagePreview = $this->imageStorage->saveContent(file_get_contents($filePreview->getPath()), $filePreview->getName(), BaseControl::DIR_IMAGE)->identifier;
            $file->updateImagePreview($imagePreview);
        }

        $this->em->persist($file);
        $this->em->flush();

        return $file;
    }

    /**
     * @param FileTag[]|array $tags
     */
    public function update(?int $id, string $name, string $content, bool $isActive, bool $isImportant, ?string $imagePreview, array $tags): File
    {
        $file = $this->get($id);

        $file->update($name, $content, $isActive, $isImportant, $imagePreview, $tags);

        $this->em->persist($file);
        $this->em->flush();

        return $file;
    }

    public function updateFile(?int $id, string $identifier, int $size, string $mimeType): File
    {
        $file = $this->get($id);

        $file->updateFile($identifier, $size, $mimeType);

        $this->em->persist($file);
        $this->em->flush();

        return $file;
    }

    public function updateName(int $id, string $name): File
    {
        $file = $this->get($id);

        $file->updateName($name);

        $this->em->persist($file);
        $this->em->flush();

        return $file;
    }

    public function get(?int $id = null): File
    {
        if ($id === null) {
            return new File();
        }

        $file = parent::get($id);

        if ($file === null) {
            return new File();
        }

        return $file;
    }

    public function remove(File $file): void
    {
        if ($file->getImagePreview() !== null) {
            $this->imageStorage->delete($file->getImagePreview());
        }

        $this->fileStorage->remove($file->getIdentifier());

        foreach ($file->getHistory() as $fileHistory) {
            $this->fileStorage->remove($fileHistory->getIdentifier());
        }

        $this->em->remove($file);
        $this->em->flush();
    }

    public function getByHash(string $hash): ?File
    {
        $criteria = ['hash' => $hash];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

    public function updateIsImportant(File $file, bool $isImportant): File
    {
        $file->setIsImportant($isImportant);

        $this->em->persist($file);
        $this->em->flush();

        return $file;
    }

    /**
     * @return File[]
     */
    public function getImportantFiles(): array
    {
        $criteria = ['isImportant' => true];
        $order    = ['lastUpdateAt' => 'DESC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $order);
    }
}
