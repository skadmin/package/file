<?php

declare(strict_types=1);

namespace Skadmin\File\Doctrine\File;

use SkadminUtils\DoctrineTraits\Entity;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class FileHistory
{
    use Entity\Id;
    use Entity\File;

    #[ORM\Column]
    private int $version;

    #[ORM\ManyToOne(targetEntity: File::class, inversedBy: 'history', cascade: ['persist'])]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private File $file;

    public function create(File $file, int $version, string $name, string $hash, string $identifier, int $size, string $mimeType): void
    {
        $this->file    = $file;
        $this->version = $version;

        $this->name       = $name;
        $this->hash       = $hash;
        $this->identifier = $identifier;
        $this->size       = $size;
        $this->mimeType   = $mimeType;
    }

    public function createFromFile(File $file): void
    {
        if ($file->getIdentifier() === null) {
            return;
        }

        $this->create($file, $file->getHistory()->count() + 1, $file->getName(), $file->getHash(), $file->getIdentifier(), $file->getSize(), $file->getMimeType());
    }

    public function getFile(): File
    {
        return $this->file;
    }

    public function getVersion(): int
    {
        return $this->version;
    }
}
