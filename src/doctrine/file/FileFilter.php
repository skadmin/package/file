<?php

declare(strict_types=1);

namespace Skadmin\File\Doctrine\File;

use Doctrine\Common\Collections\Criteria;
use Nette\SmartObject;

use SkadminUtils\DoctrineTraits\ACriteriaFilter;
use function trim;

final class FileFilter extends ACriteriaFilter
{
    use SmartObject;

    private string $phrase   = '';
    private ?bool  $isActive = null;

    public function __construct(string $phrase = '', ?bool $isActive = null)
    {
        $this->isActive = $isActive;
        $this->phrase   = $phrase;
    }

    public function getPhrase(): string
    {
        return trim($this->phrase);
    }

    public function setPhrase(string $phrase): self
    {
        $this->phrase = $phrase;

        return $this;
    }

    public function isActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function modifyCriteria(Criteria &$criteria, string $alias = 'a'): void
    {
        $expr = Criteria::expr();

        if ($this->getPhrase() !== '') {
            $criteria->andWhere(Criteria::expr()->orX(
                Criteria::expr()->contains($this->getEntityName($alias, 'name'), $this->getPhrase()),
                Criteria::expr()->contains($this->getEntityName($alias, 'content'), $this->getPhrase())
            ));
        }

        if ($this->isActive() === null) {
            return;
        }

        $criteria->andWhere(Criteria::expr()->eq('isActive', $this->isActive()));
    }
}
