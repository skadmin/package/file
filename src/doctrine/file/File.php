<?php

declare(strict_types=1);

namespace Skadmin\File\Doctrine\File;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use SkadminUtils\DoctrineTraits\Entity;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class File
{
    use Entity\Id;
    use Entity\File;
    use Entity\ImagePreview;
    use Entity\Content;
    use Entity\IsActive;

    #[ORM\Column(options: ['default' => false])]
    private bool $isImportant = false;

    /** @var Collection<int, FileHistory> */
    #[ORM\OneToMany(targetEntity: FileHistory::class, mappedBy: 'file', cascade: ['persist'])]
    #[ORM\OrderBy(['createdAt' => 'DESC'])]
    private Collection|array $history;

    /** @var Collection<int, FileTag>
     */
    #[ORM\ManyToMany(targetEntity: FileTag::class, inversedBy: 'files')]
    #[ORM\JoinTable(name: 'file_tag_rel')]
    #[ORM\OrderBy(['name' => 'ASC'])]
    private ArrayCollection|Collection|array $tags;

    private ?FileTag $firstTag = null;

    public function __construct()
    {
        $this->history = new ArrayCollection();
        $this->tags    = new ArrayCollection();
    }

    public function create(string $name, string $identifier, string $hash, int $size, string $mimeType): void
    {
        $this->name       = $name;
        $this->identifier = $identifier;
        $this->hash       = $hash;
        $this->size       = $size;
        $this->mimeType   = $mimeType;

        $this->content = '';
    }

    /**
     * @param FileTag[]|array $tags
     */
    public function update(string $name, string $content, bool $isActive, bool $isImportant, ?string $imagePreview, array $tags): void
    {
        $this->name        = $name;
        $this->content     = $content;
        $this->isImportant = $isImportant;

        $this->setTags($tags);
        $this->setIsActive($isActive);
        $this->updateImagePreview($imagePreview);
    }

    public function updateImagePreview(?string $imagePreview): void
    {
        if ($imagePreview === null || $imagePreview === '') {
            return;
        }

        $this->imagePreview = $imagePreview;
    }

    public function updateFile(string $identifier, int $size, string $mimeType): void
    {
        $newHistoryFile = new FileHistory();
        $newHistoryFile->createFromFile($this);
        $this->history->add($newHistoryFile);

        $this->identifier = $identifier;
        $this->size       = $size;
        $this->mimeType   = $mimeType;
    }

    public function updateName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param FileTag[]|array $fileTags
     */
    public function setTags(array $fileTags): void
    {
        $this->tags->clear();

        foreach ($fileTags as $fileTag) {
            $this->addTag($fileTag);
        }
    }

    public function addTag(FileTag $fileTag): void
    {
        $this->tags->add($fileTag);
    }

    public function removeTag(FileTag $fileTag): void
    {
        $this->tags->removeElement($fileTag);
    }

    /**
     * @return Collection<FileTag>
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function getFirstTags(): ?FileTag
    {
        if ($this->firstTag === null) {
            $firstTag       = $this->tags->first();
            $this->firstTag = $firstTag ? $firstTag : null;
        }

        return $this->firstTag;
    }

    public function isImportant(): bool
    {
        return $this->isImportant;
    }

    public function setIsImportant(bool $isImportant): void
    {
        $this->isImportant = $isImportant;
    }

    /**
     * @return Collection<FileHistory>
     */
    public function getHistory(): Collection
    {
        return $this->history;
    }

    public function getVersion(int $version): ?FileHistory
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('version', $version));

        $historyFile = $this->history->matching($criteria)->first();

        return $historyFile ? $historyFile : null;
    }
}
